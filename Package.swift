// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 2.1.7
let package = Package(
    name: "FreestarAds-Prebid",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Prebid",
            targets: [
                "FreestarAds-Prebid",
                "FreestarAds-Prebid-Core",
                "PrebidMobile",
                "OMSDK_Freestario"
             ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.30.0"
            ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Prebid",
            url: "https://gitlab.com/freestar/spm-freestarads-Prebid/-/raw/2.1.7/FreestarAds-Prebid.xcframework.zip",
            checksum: "3cf962b3d5487a890431144436063f7d1e97d609f9b1ea2dbec3343f60a86cd6"
            ),
        .target(
            name: "FreestarAds-Prebid-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .binaryTarget(
            name: "PrebidMobile",
            url: "https://gitlab.com/freestar/spm-freestarads-Prebid/-/raw/2.1.7/PrebidMobile.xcframework.zip",
            checksum: "57ffd8b0c1a5f3c25392e434060f89bfe807478c1df0fabd211cf8a008c41db0"
            ),
        .binaryTarget(
            name: "OMSDK_Freestario",
            url: "https://gitlab.com/freestar/spm-freestarads-Prebid/-/raw/2.1.7/OMSDK_Freestario.xcframework.zip",
            checksum: "2ebb371b683e201924eab43e6d45c5022646cb7ec974bb7ad1cd498e5b6b1c98"
            ),
    ]
)
